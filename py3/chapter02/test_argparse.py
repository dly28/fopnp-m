import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("echo", help="echo the string")
    args = parser.parse_args()
    print(args.echo)
